import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import Navbar from '../components/Navbar'
import Announcement from '../components/Announcement'
import Footer from '../components/Footer'
import NewsLetter from '../components/NewsLetter'
import { Add, Remove } from '@material-ui/icons'
import { mobile } from '../responsive'
import {useLocation} from "react-router-dom";
import {publicRequest} from "../requestMethods";
import { addProduct } from '../redux/cartRedux'
import { useDispatch } from 'react-redux'

const Container = styled.div``

const Wrapper = styled.div`
padding: 50px;
display: flex;
${mobile({padding:"10px",flexDirection:"column"})};`

const ImageContainer = styled.div`
flex:1;`

const InfoContainer = styled.div`
flex:1;
padding: 0px 50px;
${mobile({padding:"10px"})};`

const Image = styled.img`
width: 100%;
height: 90vh;
object-fit:cover;
${mobile({height:"40vh"})};`

const Title = styled.h1`
font-weight: 200;`

const Description = styled.p`
margin: 20px 0px;`

const Price = styled.span`
font-weight: 100;
font-size: 40px;`

const FilterContainer = styled.div`
display: flex;
justify-content:space-between;
width: 50%;
margin: 30px 0px;
${mobile({width:"100%"})};`

const Filter = styled.div`
display: flex;
align-items:center;`

const FilterTitle = styled.div`
font-size:20px;
font-weight: 200;`

const FilterColor = styled.div`
width: 20px;
height: 20px;
border-radius:50%;
background-color: ${props=> props.color};
margin: 0px 5px;
cursor: pointer;`

const FilterSize = styled.select`
margin-left: 10px;
`

const FilterSizeOption = styled.option``

const AddContainer = styled.div`
display: flex;
align-items:center;
width: 80%;
justify-content:space-between;
${mobile({width:"100%"})};`

const AmountContainer = styled.div`
display: flex;
align-items:center;
font-weight: 700;`

const Amount = styled.span`
width: 30px;
height: 30px;
border-radius:10px;
border: 1px solid teal;
display: flex;
align-items:center;
justify-content:center;
margin: 0px 5px;`

const Button = styled.button`
padding: 15px;
border:2px solid teal;
background-color:white;
cursor: pointer;
font-weight: 500;

&:hover{
    background-color: #f8f4f4;
}`

const Product = () => {

    const location = useLocation();
    const id = location.pathname.split("/")[2];
    const [product,setProduct] = useState({});
    const [quantity,setQuantity] = useState(1);
    const [color,setColor] = useState("");
    const [size,setSize] = useState("");
    const dispatch = useDispatch();

    useEffect(()=>{
        const getProducts = async()=>{
            try{
                const res = await publicRequest.get("/products/find/"+id);
                setProduct(res.data);
            }catch(err){}
        };
        getProducts();
    },[id]);

    const handleQuantity = (type) => {
        if(type === "dec" && quantity > 1){
            setQuantity(quantity-1);
        }else if(type==="dec" && quantity === 1){
            setQuantity(quantity);
        }else{
            setQuantity(quantity+1);
        }
    }

    const handleClick = ()=>{
        dispatch(addProduct({...product, quantity, color, size}));
    }

    return (
        <Container>
            <Navbar/>
            <Announcement/>
            <Wrapper>
                <ImageContainer>
                    <Image src={product.img}/>
                </ImageContainer>
                <InfoContainer>
                    <Title>{product.title}</Title>
                    <Description>asset index.html 741 bytes [emitted]
                    Entrypoint main 1.94 MiB (1.92 MiB) = static/js/bundle.js 1.93 MiB main.391a0fd8eeafb2aea26f.hot-update.js 6.28 KiB 2 auxiliary assets
                    cached modules 5.64 MiB [cached] 6148 modules
                    runtime modules 28.1 KiB 13 modules</Description>
                    <Price>Rs. {product.price}</Price>
                    <FilterContainer>
                        <Filter>
                            <FilterTitle>Color</FilterTitle>
                            {product.color?.map((c)=>(
                                <FilterColor color={c} key={c} onClick={()=>setColor(c)}/>
                            ))}
                        </Filter>
                        <Filter>
                            <FilterTitle>Size</FilterTitle>
                            <FilterSize onChange={(e)=>setSize(e.target.value)}>
                                {product.size?.map((s)=>(
                                    <FilterSizeOption>{s}</FilterSizeOption>
                                ))}
                            </FilterSize>
                        </Filter>
                    </FilterContainer>
                    <AddContainer>
                        <AmountContainer>
                            <Remove onClick={()=>handleQuantity("dec")}/>
                            <Amount>{quantity}</Amount>
                            <Add onClick={()=>handleQuantity("inc")}/>
                        </AmountContainer>
                        <Button onClick={handleClick}>Add to cart</Button>
                    </AddContainer>
                </InfoContainer>
            </Wrapper>
            <NewsLetter/>
            <Footer/>
        </Container>
    )
}

export default Product
