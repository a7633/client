import { Facebook, Instagram, MailOutline, Phone, Pinterest, Room, Twitter } from '@material-ui/icons'
import React from 'react'
import styled from 'styled-components'
import { mobile } from '../responsive'

const Container = styled.div`
display: flex;
${mobile({flexDirection:"column"})};`

const Left = styled.div`
flex:1;
display: flex;
flex-direction:column;
padding: 20px;`

const Logo = styled.h1``

const Desc = styled.p`
margin: 20px 0px;`

const  SocialContainer = styled.div`
display: flex;`

const SocialIcon = styled.div`
width: 40px;
height: 40px;
border-radius:50%;
color:white;
background-color:#${(props)=>props.color};
display: flex;
align-items:center;
justify-content:center;
margin: 20px;`

const Center = styled.div`
flex:1;
padding: 20px;
${mobile({display:"none"})};`

const Title = styled.h1`
margin-bottom:30px;`

const List = styled.ul`
margin: 0;
padding: 0;
list-style:none; 
display: flex;
flex-wrap:wrap;` 

const ListItem = styled.li`
width: 50%;
margin-bottom: 10px;`

const Right = styled.div`
flex:1;
padding: 20px;
${mobile({backgroundColor:"#d6cece"})};`

const ContactItem = styled.div`
margin-bottom: 20px;
display: flex;
align-items:center;`
    
const Payment = styled.img`
width: 100%;`
    
const Footer = () => {
    return (
        <Container>
            <Left>
                <Logo>
                    AMAZON
                </Logo>
                <Desc>
                    Entrypoint main 1.9 MiB (1.91 MiB) = static/js/bundle.js 1.89 MiB main.b244c326f1ec74a3e0d6.hot-update.js 5.92 KiB 2 auxiliary assets
                    cached modules 5.62 MiB [cached] 6145 modules
                    runtime modules 28.1 KiB 13 modules./src/components/NewsLetter.jsx 3.48 KiB [built] [code generated]
                    webpack 5.65.0 compiled successfully in 838 ms
                </Desc>
                <SocialContainer>
                    <SocialIcon color="3B5999">
                        <Facebook/>
                    </SocialIcon>
                    <SocialIcon color="E4405F">
                        <Instagram/>
                    </SocialIcon>
                    <SocialIcon color="55ACEE">
                        <Twitter/>
                    </SocialIcon>
                    <SocialIcon color="E60023">
                        <Pinterest/>
                    </SocialIcon>
                </SocialContainer>
            </Left>
            <Center>
                <Title>Useful links</Title>
                <List>
                    <ListItem>Home</ListItem>
                    <ListItem>Cart</ListItem>
                    <ListItem>Man Fashion</ListItem>
                    <ListItem>Woman Fashion</ListItem>
                    <ListItem>Accesories</ListItem>
                    <ListItem>My Account</ListItem>
                    <ListItem>Order Tracking</ListItem>
                    <ListItem>Wishlist</ListItem>
                    <ListItem>Terms</ListItem>
                </List>
            </Center>
            <Right>
                <Title>Contact</Title>
                <ContactItem>
                    <Room style={{marginRight:"10px"}}/>
                    A888 Chincwad Plaza, South Region 422012
                </ContactItem>
                <ContactItem>
                    <Phone style={{marginRight:"10px"}}/>
                    +91 896 233 1544
                </ContactItem>
                <ContactItem>
                    <MailOutline style={{marginRight:"10px"}}/>
                    contactemail@gmail.com
                </ContactItem>
                <Payment src="https://i.ibb.co/Qfvn4z6/payment.png"/>
            </Right>
        </Container>
    )
}

export default Footer
