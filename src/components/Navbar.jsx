import React from 'react'
import styled from 'styled-components'
import SearchIcon from '@material-ui/icons/Search';
import { Badge } from '@material-ui/core';
import {mobile} from "../responsive"
import {useSelector} from "react-redux";
import ShoppingCartOutlined from '@material-ui/icons/ShoppingCartOutlined';
import { Link } from 'react-router-dom';

const Container = styled.div`
height:60px;
${mobile({height:"50px"})};`

const Wrapper = styled.div`
padding: 10px 20px;
display: flex;
justify-content: space-between;
align-items: center;
${mobile({padding:"10px 0px"})};`

//Left
const Left = styled.div`
flex: 1;
display: flex;
align-items: center;
`
const Language = styled.span`
font-size: 14px;
font-weight: 600;
cursor: pointer;
${mobile({display:"none"})};`

const SearchContainer = styled.div`
border: 1px solid lightgray;
display: flex;
align-items: center;
margin-left: 25px;
padding: 5px;
border-radius: 25px;
`
const Input = styled.input`
border: none;
${mobile({width:"0px"})};`

//Center
const Center = styled.div`
flex: 2;
text-align: center;
${mobile({flex:1})};`

const Logo = styled.h1`
font-weight:bold;
${mobile({fontSize:"24px"})};`

//Right
const Right = styled.div`
flex: 1;
display: flex;
justify-content: flex-end;
${mobile({flex:2,justifyContent:"center"})};`

const MenuItem = styled.div`
font-size: 15px;
font-weight: 600;
cursor: pointer;
margin-left: 25px;
${mobile({fontSize:"12px", marginLeft:"10px"})};`

const Navbar = () => {

  const quantity = useSelector(state => state.cart.quantity);

    return (
        <Container>
            <Wrapper>
                <Left>
                    <Language>ENG</Language>
                    <SearchContainer>
                        <Input placeholder='search'/> 
                        <SearchIcon style={{color:"gray", fontSize:"16px"}}/>
                    </SearchContainer>
                </Left>
                <Center>
                    <Logo>
                        AMAZON
                    </Logo>
                </Center>
                <Right>
                    <Link to="/logout" style={{textDecoration:"none"}}>
                    <MenuItem>Logout</MenuItem>
                    </Link>
                    <Link to="/cart">
                    <MenuItem>
                        <Badge badgeContent={quantity} color="primary">
                            <ShoppingCartOutlined/>
                        </Badge>
                    </MenuItem>
                    </Link>
                </Right>
            </Wrapper>
        </Container>
    )
}

export default Navbar
